import React,{useState,useEffect} from 'react'

// Bootstrap
import {Form, Button, Container} from 'react-bootstrap'

// Sweetalert
import Swal from 'sweetalert2'

// Redirect
import {Redirect} from 'react-router-dom'

export default function AddReview(){

	// States
	const [fullName, setFullName] = useState("")
	const [email, setEmail] = useState("")
	const [rating, setRating] = useState("")
	const [review, setReview] = useState("")
	const [isActive, setIsActive] = useState(false)
	const [willRedirect, setWillRedirect] = useState(false)

	// Use Effect
	useEffect(()=> {

		if((fullName !== "" && email !== "" && rating !== "" && review !== "") && (rating >= 1 && rating <= 5)){

				setIsActive(true)
		} else {

			setIsActive(false)

		}

	},[fullName,email,rating,review])

	function addReview(e){

		e.preventDefault()

		fetch('https://immense-harbor-69013.herokuapp.com/gr/reviews', {

			method: "POST",
			headers: {

				'Content-Type' : 'application/json',

			},
			body: JSON.stringify({

				fullName: fullName,
				email: email,
				rating: rating,
				review: review

			})

		}).then(response => response.json()).then(data => {

			if(!data.addedData){

				Swal.fire({

					icon: "error",
					title: "Fail to add a review",
					text: data.message

				})

			} else {

				Swal.fire({

					icon: "success",
					title: "SUCCESS!",
					text: data.message

				})

				setWillRedirect(true)

			}

		})

		setFullName("")
		setEmail("")
		setRating("")
		setReview("")

	}

	function backToReviewLists(){

		setWillRedirect(true);

	}

	return(

		willRedirect
		?
		<Redirect to="/" />
		:
		<Container>
			<h1 id="game-name">Grand Theft Auto 5</h1>
			<Form onSubmit={e => addReview(e)}>
				<Form.Group>
					<Form.Label>Full Name</Form.Label>
					<Form.Control type="text" placeholder="Enter your Full Name" value={fullName} onChange={e => {setFullName(e.target.value)}} required/>
				</Form.Group>
				<Form.Group>
					<Form.Label>Email</Form.Label>
					<Form.Control type="email" placeholder="Enter your Email" value={email} onChange={e => {setEmail(e.target.value)}} required/>
				</Form.Group>
				<Form.Group>
					<Form.Label>Rating</Form.Label>
					<Form.Control type="number" placeholder="1 is the lowest, 5 is the highest" min="1" max="5" value={rating} onChange={e => {setRating(e.target.value)}} required/>
				</Form.Group>
				<Form.Group>
					<Form.Label>Review</Form.Label>
					<Form.Control as="textarea" rows="5" value={review} placeholder="Enter your Comment Here" onChange={e => {setReview(e.target.value)}} required/>
				</Form.Group>
				{
					isActive
					?
					<Button id="add-review-button" variant="primary" size="lg" type="submit">Add Review</Button>
					:
					<Button id="add-review-button" variant="primary" size="lg" type="submit" disabled>Add Review</Button>
				}
			</Form>
			<Button id="back-review" variant="success" size="mg" onClick={backToReviewLists}>Back To Review Lists</Button>
		</Container>

		)

}