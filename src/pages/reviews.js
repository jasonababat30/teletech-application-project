import React, {useState, useEffect} from 'react'

// Bootstrap
import {Table, Button, Container} from 'react-bootstrap'

// Redirect
import {Redirect} from 'react-router-dom'

export default function Reviews(){

	// State
	const [allReviews, setAllReviews] = useState([])
	const [willRedirect, setWillRedirect] = useState(false)

	// Use Effect

	useEffect(() => {

		fetch('https://immense-harbor-69013.herokuapp.com/gr/reviews').then(response => response.json()).then(data => {

			setAllReviews(data.data)

		})

	})

	let reviewsAll = allReviews.map(review => {

		return(

			<tr id={review._id}>
				<td>{review.fullName}</td>
				{
					review.rating === 5
					?
					<td id="rating-5">{review.rating}</td>
					:
						review.rating === 4
						?
						<td id="rating-4">{review.rating}</td>
						:
							review.rating === 3
							?
							<td id="rating-3">{review.rating}</td>
							:
								review.rating === 2
								?
								<td id="rating-2">{review.rating}</td>
								:
									<td id="rating-1">{review.rating}</td>
				}
				<td>{review.review}</td>
				<td>{review.createdAt}</td>
			</tr>

			)

	})

	function createReview(){

		setWillRedirect(true)

	}

	return(

		willRedirect
		?
		<Redirect to="/addReview" />
		:
		<Container>
			<h1 id="game-title">Grand Theft Auto 5 Reviews</h1>
			<Button id="go-add-review" variant="success" onClick={createReview}>Add Review</Button>
			<Table>
				<thead>
					<tr>
						<th>Full Name</th>
						<th>Rating</th>
						<th>Review</th>
						<th>Added On</th>
					</tr>
				</thead>
				<tbody>
					{reviewsAll}
				</tbody>
			</Table>
		</Container>

		)

}