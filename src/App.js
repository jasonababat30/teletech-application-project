import React from 'react';
import 'bootstrap/dist/css/bootstrap.min.css';
import './App.css'

// Import react-router-dom
import {BrowserRouter as Router} from 'react-router-dom'
import {Route,Switch} from 'react-router-dom'

// Components and Pages
import Reviews from './pages/reviews'
import AddReview from './pages/addReview'



function App(){

  return(

    <>
      <Router>
        <Switch>
          <Route exact path="/" component={Reviews} />
          <Route exact path="/addReview" component={AddReview} />
        </Switch>
      </Router>
    </>

    )

}

export default App
